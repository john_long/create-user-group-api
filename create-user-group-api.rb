require 'gitlab'
require 'io/console'

client = Gitlab.client(
  endpoint: "#{ENV['EXTERNAL_URL'].match(/\/?$/) ? ENV['EXTERNAL_URL'] + '/' : ENV['EXTERNAL_URL']}api/v4",
  private_token: ENV['PRIVATE_TOKEN']
)

loop_conditional = true

def enter_password(message = "")
    if message.length > 0
        print "\n#{message}\n"
    end
    print "Enter password: "
    password = STDIN.noecho(&:gets).chomp
    print "\nConfirm password: "
    confirm_password = STDIN.noecho(&:gets).chomp
    return password == confirm_password ? password : false
end

def create_user(client, message = "")
    if message.length > 0
        print "\n#{message}\n"
    end
    print "Enter email address: "
    email = gets.chomp
    email =~ URI::MailTo::EMAIL_REGEXP ? true : create_user(client, "Not a valid email address. Try again...")

    password = enter_password()
    if !password
        enter_password("Password does not match")
    end
    print "\nEnter Username: "
    username = gets.chomp
    print "Enter Name of User: "
    user = gets.chomp
    client.users.each do |u|
        if u.email == email
            message = "Email address already in use. Try again..."
            create_user(client, message)
        elsif u.username == username
            message = "Username already in use. Try again..."
            create_user(client, message)
        end
    end
    client.create_user(email, password, username, name: user)
    puts "Created user #{username} with email address #{email}"
    print "Press [ENTER] to continue..."
    gets.chomp
end

def create_group(client, message = "")
    if message.length > 0
        print "\n#{message}\n"
    end
    print "Enter group name: "
    group = $stdin.gets.chomp
    print "\nEnter group path(Press [ENTER] for default: #{group.downcase.gsub(/[^0-9A-Za-z]/, '')}): "
    group_path = $stdin.gets.chomp
    group_path.length > 0 ? true : (group_path = group.downcase.gsub(/[^0-9A-Za-z]/, ''))
    client.groups.each do |g|
        if g.path == group_path
            create_group(client, "Group path already in use. Try again...")
        end
    end
    client.create_group(group, group_path)
end

while loop_conditional do
    system "clear"
    puts "What would you like to do?\n[1] Create user\n[2] Create group\n[q] to quit"
    option = gets.chomp
    case option
    when "1"
        create_user(client)
    when "2"
        create_group(client)
    when "q"
        loop_conditional = false
    end
end