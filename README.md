# GitLab Create User/Group API integration

This script requires the Ruby gem `gitlab` be installed.

To run the script, execute the following in a terminal:

```bash
EXTERNAL_URL='<external_url_for_gitlab_instance>' PRIVATE_TOKEN='<personal_access_token>' ruby create-user-group-api.rb
```
